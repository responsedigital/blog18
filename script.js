class Blog18 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initBlog18()
    }

    initBlog18 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: blog18")
    }

}

window.customElements.define('fir-blog-18', Blog18, { extends: 'section' })
