<?php

namespace Fir\Pinecones\Blog18\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Blog18 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Blog Posts';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Blog18.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['blog18'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
            'post_type' => [
                'news' => 'News',
                'services' => 'Services',
                'projects' => 'Projects'
            ],
            'preview' => false,
            'bg_color' => 'white'
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        global $wp_query;
        $data['paged'] =  ($_GET['news-page']) ?  intval($_GET['news-page']) : 1;
        $data['post_type'] = ($data['content']['post_type']) ?: 'posts';
        $data['preview'] = ($data['content']['preview']) ?: $this->defaults['content']['preview'];
        $postNum = $data['preview'] ? 3 : 6;
        $data['args'] = array(
            'posts_per_page' => $postNum,
            'paged' => $data['paged'],
            'post_type' => $data['post_type']
        );
        $wp_query = new \WP_Query( $data['args'] );
        $data['posts'] = $wp_query;
        $data['total'] = $data['posts']->max_num_pages;
        $data['custom'] = \Fir\Utils\Helpers::getMeta($data['posts']->posts);
        wp_reset_query();

        $data['bg_color'] = ($data['content']['bg_color']) ?: $this->defaults['content']['bg_color'];
        $data['flip'] = $data['options']['flip_horizontal'] ? 'blog-18--flip' : '';
        $data['text_align'] = 'blog-18--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $Blog18 = new FieldsBuilder('Blog Posts');

        $Blog18
            ->addGroup('content', [
                'label' => 'Blog Posts',
                'layout' => 'block'
            ])
                ->addSelect('post_type', [
                    'choices' => $this->defaults['content']['post_type']
                ])
                ->addSelect('bg_color', [
                    'choices' => [
                        'white' => 'White',
                        'gray' => 'Gray',
                        'blue' => 'Blue',
                        'teal' => 'Teal'
                    ]
                ])
                ->addTrueFalse('preview')
                    ->setInstructions("Should this block be limited to 3 posts - used for previewing smaller number of posts in a page.")
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $Blog18->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/Blog18/style.css')->uri(), false, null);
    }
}
