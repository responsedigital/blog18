<!-- Start blog18 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A three column layout for posts. -->
@endif
<section class="{{ $block->classes }} {{ $bg_color }}" is="fir-blog-18" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="blog-18 {{ $theme }} {{ $text_align }} {{ $flip }}">
    <div class="blog-18__wrapper">
      @foreach($posts->posts as $index=>$x)
        <div class="blog-18__post">
          @if($custom[$index]['news_info']['external'])
          <h6 class="blog-18__post-category">Industry</h6>
          @elseif(get_blog_details()->blog_id == 1)
          <h6 class="blog-18__post-category">SkyeTec</h6>
          @else
          <h6 class="blog-18__post-category">SkyeTec Engineering</h6>
          @endif
          <h3 class="blog-18__post-tile">{{ $x->post_title }}</h3>
          <div class="blog-18__post-description">
            {!! apply_filters('the_excerpt', $custom[$index]['news_info']['preview']) !!}
          </div>
          @if($custom[$index]['news_info']['external'])
          <a href="{{ $custom[$index]['news_info']['article']['url'] }}" target="_blank">Link To Article<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="long-arrow-right" class="svg-inline--fa fa-long-arrow-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M295.515 115.716l-19.626 19.626c-4.753 4.753-4.675 12.484.173 17.14L356.78 230H12c-6.627 0-12 5.373-12 12v28c0 6.627 5.373 12 12 12h344.78l-80.717 77.518c-4.849 4.656-4.927 12.387-.173 17.14l19.626 19.626c4.686 4.686 12.284 4.686 16.971 0l131.799-131.799c4.686-4.686 4.686-12.284 0-16.971L312.485 115.716c-4.686-4.686-12.284-4.686-16.97 0z"></path></svg></a>
          @else
          <a href="/news/{{ $x->post_name }}">Read More<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="long-arrow-right" class="svg-inline--fa fa-long-arrow-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M295.515 115.716l-19.626 19.626c-4.753 4.753-4.675 12.484.173 17.14L356.78 230H12c-6.627 0-12 5.373-12 12v28c0 6.627 5.373 12 12 12h344.78l-80.717 77.518c-4.849 4.656-4.927 12.387-.173 17.14l19.626 19.626c4.686 4.686 12.284 4.686 16.971 0l131.799-131.799c4.686-4.686 4.686-12.284 0-16.971L312.485 115.716c-4.686-4.686-12.284-4.686-16.97 0z"></path></svg></a>
          @endif
        </div>
      @endforeach
    </div>

    @if(!$preview && $total > 1)
      @include('Blog25.view', [
        'paged' => $paged
      ])
    @endif
  </div>
</section>
<!-- End blog18 -->
